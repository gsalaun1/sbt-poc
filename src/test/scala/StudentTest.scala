import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.assertEquals

class StudentTest {

  @Test
  def should_init_with_correct_name() {
    val student = new Student();
    assertEquals(student.name, "Student name")
  }

}
