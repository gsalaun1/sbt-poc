# POC SBT

[![pipeline status](https://gitlab.com/gsalaun1/sbt-poc/badges/main/pipeline.svg)](https://gitlab.com/gsalaun1/sbt-poc/-/commits/main)


[![coverage report](https://gitlab.com/gsalaun1/sbt-poc/badges/main/coverage.svg)](https://gitlab.com/gsalaun1/sbt-poc/-/commits/main) 

## Lancer l'application

`sbt run`

## Lancer les tests

`sbt test`

## Lancer les tests avec génération du rapport de couverture

`sbt jacoco`

## Doc

* Getting started with sbt : https://docs.scala-lang.org/getting-started/sbt-track/getting-started-with-scala-and-sbt-on-the-command-line.html

* Dependency Tree : https://www.baeldung.com/scala/sbt-dependency-tree \
`sbt dependencyTree`

* How to: GitLab test coverage with JaCoCo and Gradle : https://akobor.me/posts/how-to-gitlab-test-coverage-with-jacoco-and-gradle

* A tester : https://docs.gitlab.com/ee/ci/pipelines/settings.html#add-test-coverage-results-using-coverage-keyword